package dbutil;

import java.sql.Connection;

public class Main {

    public static void main(String[] args) throws Exception
    {
      Connection conn = DBUtilClass.getConnection();
      System.out.println("Connection ready");
      DBUtilClass.Close(conn);
    }
}
